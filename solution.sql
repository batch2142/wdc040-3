INSERT INTO users (email, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", now());

INSERT INTO users (email, password, datetime_created)
VALUES ("juandelacruz@gmail.com", "passwordB", now());

INSERT INTO users (email, password, datetime_created)
VALUES ("janesmith@gmail.com", "passwordC", now());

INSERT INTO users (email, password, datetime_created)
VALUES ("mariadelacruz@gmail.com", "passwordD", now());

INSERT INTO users (email, password, datetime_created)
VALUES ("johndoe@gmail.com", "passwordE", now());





INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES ("1", "First Code", "Hello World!", now());

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES ("3", "Second Code", "Hello Earth!", now());

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES ("4", "Third Code", "Welcome to Mars!", now());

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES ("6", "Fourth Code", "Bye bye solar system!", now());


SELECT * FROM posts
WHERE author_id = 1;

SELECT email, datetime_created FROM users;

UPDATE posts
SET content = "Hello to the people of the Earth"
WHERE id = 2;

DELETE FROM users
WHERE email = "johndoe@gmail.com";




